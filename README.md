# Frontend Mentor - Pod request access landing page solution

This is a solution to the [Pod request access landing page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/pod-request-access-landing-page-eyTmdkLSG). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover states for interactive elements
- Receive an error message when the form is submitted if:
  - The `Email address` field is empty should show "Oops! Please add your email"
  - The email is not formatted correctly should show "Oops! Please check your email"

### Screenshot

![Mobile](./screenshots/mobile.jpg)
![Mobile Errors](./screenshots/mobile-error.jpg)
![Desktop](./screenshots/desktop.jpg)
![Desktop Errors](./screenshots/desktop-error.jpg)

### Links

-   Solution URL: [Gitlab Repository](https://gitlab.com/frontend-mentor-challenge/17-pod-request-access-landing-page)
-   Live Site URL: [Live](https://pod-request-access-landing-page.onrender.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Styled Components](https://styled-components.com/) - For styles
- [Formik](https://formik.org/) - For Form

### What I learned

### Continued development

### Useful resources

-   [Figma](https://figma.com) - For design
-   [React](https://es.react.dev/reference/react) - For resolving doubts

## Author

-   Website - [Issac Leyva](In construction)
-   Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments
