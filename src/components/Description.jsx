import styled from "styled-components"

const P = styled.p `
    margin-top: 1.6rem;
    color: #C2CBE5;
    text-align: center;
    font-size: 1.5rem;
    font-style: normal;
    font-weight: 300;
    line-height: 25px;

    @media (min-width: 768px) {
      margin-top: 3.1rem;
      text-align: left;
      font-size: 1.8rem;
      line-height: 28px;
      width: 44.5rem;
      order: 2;
    }

    @media (min-width: 1440px) {
      margin-top: 2.4rem;
    }
`

const Description = () => {
  return (
    <>
        <P>
            Upload your audio to Pod with a single click. We’ll then distribute your podcast to Spotify, Apple Podcasts, Google Podcasts, Pocket Casts and more!
        </P>
    </>
  )
}

export default Description