import styled from "styled-components"

import imgSpotify from '../assets/desktop/spotify.svg'
import imgApple from '../assets/desktop/apple-podcast.svg'
import imgGoogle from '../assets/desktop/google-podcasts.svg'
import imgPocket from '../assets/desktop/pocket-casts.svg'

const ContentBrands = styled.div `
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 3.3rem;

    @media (min-width: 768px) {
      margin-top: 6.4rem;
      width: 53.6rem;
      order: 4;
    }

    @media (min-width: 1440px) {
      margin-top: 6.4rem;
    }
`

const Spotify = styled.img `
  width: 5.6rem;
  height: 1.7rem;

  @media (min-width: 768px) {
    width: 9.6rem;
    height: 2.9rem;
  }
`

const Apple = styled.img `
  width: 5.6rem;
  height: 1.7rem;

  @media (min-width: 768px) {
    width: 7.8rem;
    height: 2.9rem;
  }
`

const Google = styled.img `
  width: 5.6rem;
  height: 1.7rem;

  @media (min-width: 768px) {
    width: 12.5rem;
    height: 1.8;
  }
`

const Pocket = styled.img `
  width: 5.6rem;
  height: 1.7rem;

  @media (min-width: 768px) {
    width: 12.9rem;
    height: 2.6rem;
  }
`

const Brands = () => {
  return (
    <ContentBrands>
        <Spotify src={imgSpotify} alt="Spotify Brand Image" />
        <Apple src={imgApple} alt="Apple Brand Image" />
        <Google src={imgGoogle} alt="Google Brand Image" />
        <Pocket src={imgPocket} alt="Pocket Brand Image" />
    </ContentBrands>
  )
}

export default Brands