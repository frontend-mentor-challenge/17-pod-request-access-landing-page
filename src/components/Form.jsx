import { Formik } from "formik"
import styled from "styled-components"

const AccessForm = styled.form`
  margin-top: 4.8rem;
  width: 100%;

  @media (min-width: 768px) {
    position: relative;
    margin-top: 4rem;
    width: 42.7rem;
    order: 3;
  }

  @media (min-width: 1440px) {
    margin-top: 4rem;
  }
`

const Input = styled.input`
  width: 100%;
  height: 4.6rem;
  flex-shrink: 0;
  padding: .9rem 3.2rem;
  border: none;
  border-radius: 2.8rem;
  background-color: #2C344B;
  color: #FFFFFF;
  font-size: 1.4rem;
  font-weight: bold;
  line-height: 28px;

  @media (min-width: 768px) {
    height: 5.6rem;
    padding: 1.4rem 3.2rem;
  }
`

const Button = styled.button`
  margin-top: 1.6rem;
  width: 100%;
  height: 4.6rem;
  flex-shrink: 0;
  padding: .9rem auto;
  border: none;
  border-radius: 2.8rem;
  background-color: #54E6AF;
  box-shadow: 0px 25px 20px -20px rgba(84, 230, 175, 0.50);
  color: #121725;
  font-size: 1.4rem;
  font-weight: bold;
  line-height: 28px;

  @media (min-width: 768px) {
    position: absolute;
    width: 16.2rem;
    margin: 0;
    top: 5px;
    right: 5px;
  }

  @media (min-width: 1440px) {
    &:hover {
      background-color: #B3FFE2;
      cursor: pointer;
    }
  }
`

const MessageErrors = styled.span `
  margin: .8rem 0 0 3.2rem;
  color: #FB3E3E;
  font-size: 1.2rem;
`

const Form = () => {
  return (
    <Formik
      initialValues={{email:''}}
      validate={values => {
        const errors = {}
        if (!values.email) {
          errors.email = 'Oops! Please add your email'
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Oops! Please check your email'
        }
        return errors
      }}
      onSubmit={(values, {setSubmitting}) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2))
          setSubmitting(false)
        }, 400);
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <AccessForm noValidate onSubmit={handleSubmit}>
          <Input
            type="email"
            name="email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
            placeholder="Email Address"
          />
            <MessageErrors>{errors.email && touched.email && errors.email}</MessageErrors>
          <Button
            type="submit"
            disabled={isSubmitting}
          >
            Request Access
          </Button>
        </AccessForm>
      )}
    </Formik>
    
  )
}

export default Form