import styled from "styled-components"

const OverlayDiv = styled.div `
    background-color: #54E6AF;
    opacity: 0.7993;
    mix-blend-mode: multiply;
`

const Overlay = () => {
  return (
    <OverlayDiv />
  )
}

export default Overlay