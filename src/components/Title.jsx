import styled from "styled-components"

const H1 = styled.h1 `
    color: #54E6AF;
    font-size: 2.6rem;
    font-weight: 300;
    text-align: center;
    line-height: 38px;
    text-transform: uppercase;  

    @media (min-width: 768px) {
      font-size: 4.8rem;
      text-align: left;
      line-height: 56px;
      order: 1;
    }

    @media (min-width: 1440px) {
      font-size: 5.2rem;
      line-height: 62px;
    }
`

const Span = styled.span `
    color: #FFFFFF;
`

const Title = () => {
  return (
    <>
       <H1>Publish your podcasts <Span>everywhere.</Span></H1> 
    </>
  )
}

export default Title