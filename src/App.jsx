import styled from 'styled-components'
import mobileBackground from './assets/mobile/image-host.jpg'
import tabletBackground from './assets/tablet/image-host.jpg'
import desktopBackground from './assets/desktop/image-host.jpg'
import patternDots from './assets/desktop/bg-pattern-dots.svg'
import imageLogo from './assets/desktop/logo.svg'
import Title from './components/Title'
import Description from './components/Description'
import Brands from './components/Brands'
import Form from './components/Form'

const Wrapper = styled.main`
  height: 66.7rem;
  height: 100vh;
  background-image: url(${mobileBackground});
  background-repeat: no-repeat;
  background-size: cover;

  @media (min-width: 768px) {
    max-height: 102.4rem;
    background-image: url(${tabletBackground}), url(${patternDots});
    background-size: 49.1rem 76.7rem, 23.2rem 10.4rem;
    background-position: top right, 30px bottom;
  }

  @media (min-width: 1440px) {
    background-image: url(${patternDots}), url(${desktopBackground});
    background-size: 23.2rem 10.4rem, 88.8rem 64rem;
    background-position: right 720px, right 130px;
  }
`

const LogoContainer = styled.div`
  margin: 0 auto;
  padding-top: 6.2rem;
  text-align: center;

  @media (min-width: 768px) {
    width: auto;
    padding: 5rem 0 0 3.9rem;
    text-align: left;
  }

  @media (min-width: 1440px) {
    padding: 10.2rem 0 0 16.5rem;
  }
`

const Logo = styled.img`

`

const Section = styled.section`
  margin: 5.7rem 2.4rem 0 2.4rem;

  @media (min-width: 768px) {
    margin: 15.2rem 0 0 3.9rem;
    width: 63.5rem;
    height: 50.9rem;
    background-color: #121725;
    padding-top: 9.3rem;
    display: flex;
    flex-direction: column;
  }

  @media (min-width: 1440px) {
    margin: 10.3rem 0 0 16.5rem;
    width: 72.3rem;
    padding-top: 8.8rem;
  }
`

function App() {

  return (
    <Wrapper>
      <LogoContainer>
        <Logo src={imageLogo} alt='Page Logo' />
      </LogoContainer>
      <Section>
        <Title />
        <Description />
        <Brands />
        <Form />
      </Section>
    </Wrapper>
  )
}

export default App
